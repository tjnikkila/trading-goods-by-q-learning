import argparse

from tradeEnvironment import TradeEnvironment

parser = argparse.ArgumentParser()
parser.add_argument("--epicCount", help="How many epic to run until saving Q-table and exiting",
                    type=int, required=False, default=10000)
parser.add_argument("--updateQTable", help="Update existing Q-table parameters. If false or not given, then overrides existing Q-table values",
                    type=bool, required=False)
parser.add_argument("--filePath", help="Where to look for and save to Q-table values",
                    type=str, required=False, default='./qtable.json')
parser.add_argument("--alpha", help="Value for alpha parameter in formula \"Q'[s, a] = (1 - α) · Q[s, a] + α · (r + γ · Q[s', argmaxa'(Q[s', a'])])\"",
                    type=float, required=False, default=0.2)
parser.add_argument("--gamma", help="Value for gamma parameter in formula \"Q'[s, a] = (1 - α) · Q[s, a] + α · (r + γ · Q[s', argmaxa'(Q[s', a'])])\"",
                    type=float, required=False, default=0.2)
parser.add_argument("--exploitationProb", help="Propability for exloitation. Values should be bertween 0 and 1. Probability for exploration is 1 - exploitationProb.",
                    type=float, required=False, default=0.67)
parser.add_argument("--repeatCount", help="How many times to repeat with same settings. Result files indexing is growing from one.",
                    type=int, required=False, default=1)
parser.add_argument("--randomInitState", help="If set to False, then every epic start from state 'notebook'. If True, then epic start state is random.",
                    type=bool, required=False, default=False)
parser.add_argument("--stateRewards", help="Original state rewards can be override by this parameter. Example: {\"mobile\": 10,\"pc\": 11}.",
                    type=str, required=False, default=None)
parser.add_argument("--stateActions", help="Original state actions can be override by this parameter. Example: [{'action':'dealerA': [{'state': 'Mobile', 'transitions': [{'name': 'final', 'prob': 0.3}, {'name': 'pc', 'prob': 0.7}}] }].",
                    type=str, required=False, default=None)

args = parser.parse_args()
epicCount = args.epicCount
updateQTable = args.updateQTable
filePath = args.filePath
alpha = args.alpha
gamma = args.gamma
exploitationProb = args.exploitationProb
repeatCount = args.repeatCount
randomInitState = args.randomInitState
stateRewards = args.stateRewards
stateActions = args.stateActions


TradeEnvironment(epicCount, updateQTable, filePath, alpha, gamma, exploitationProb, repeatCount, randomInitState, stateRewards, stateActions)()
