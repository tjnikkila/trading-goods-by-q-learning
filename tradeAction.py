import copy
from pprint import pprint

dealerAName = 'A'
dealerBName = 'B'
dealerCName = 'C'

class EfficiencyActions:
    def __init__(self, states):
        finalState = states.finalState
        laptop = states.laptop
        pc = states.pc
        mobile = states.mobile
        calculator = states.calculator
        notebook = states.notebook
        self.dealerA = {
            'name': dealerAName,
            'decription': 'Good dealer',
            'index': 0,
            'stateTransition': {
                finalState['name']: [],
                laptop['name']: [{'name': finalState['name'], 'prob': 0.7}, {'name': laptop['name'], 'prob': 0.3}],
                pc['name']: [{'name': laptop['name'], 'prob': 0.7}, {'name': pc['name'], 'prob': 0.3}],
                mobile['name']: [{'name': pc['name'], 'prob': 0.7}, {'name': mobile['name'], 'prob': 0.3}],
                calculator['name']: [{'name': mobile['name'], 'prob': 0.7}, {'name': calculator['name'], 'prob': 0.3}],
                notebook['name']: [{'name': calculator['name'], 'prob': 0.7}, {'name': notebook['name'], 'prob': 0.3}]
            }}
        self.dealerB = {
            'name': dealerBName,
            'decription': 'Bad dealer',
            'index': 1,
            'stateTransition': {
                finalState['name']: [],
                laptop['name']: [{'name': finalState['name'], 'prob': 0.3}, {'name': laptop['name'], 'prob': 0.7}],
                pc['name']: [{'name': laptop['name'], 'prob': 0.3}, {'name': pc['name'], 'prob': 0.7}],
                mobile['name']: [{'name': pc['name'], 'prob': 0.3}, {'name': mobile['name'], 'prob': 0.7}],
                calculator['name']: [{'name': mobile['name'], 'prob': 0.3}, {'name': calculator['name'], 'prob': 0.7}],
                notebook['name']: [{'name': calculator['name'], 'prob': 0.3}, {'name': notebook['name'], 'prob': 0.7}]
            }}
        self.dealerC = {
            'name': dealerCName,
            'decription': 'Retired',
            'index': 2,
            'stateTransition': {
                finalState['name']: [],
                laptop['name']: [],
                pc['name']: [],
                mobile['name']: [],
                calculator['name']: [],
                notebook['name']: []
            }}

class DefaultActions:
    def __init__(self, states):
        finalState = states.finalState
        laptop = states.laptop
        pc = states.pc
        mobile = states.mobile
        calculator = states.calculator
        notebook = states.notebook
        self.dealerA = {
            'name': dealerAName,
            'decription': 'risk taking',
            'index': 0,
            'stateTransition': {
                finalState['name']: [],
                laptop['name']: [{'name': finalState['name'], 'prob': 0.3}, {'name': pc['name'], 'prob': 0.4},{ 'name': mobile['name'], 'prob': 0.3}],
                pc['name']: [{'name': finalState['name'], 'prob': 0.1}, {'name': laptop['name'], 'prob': 0.5}, {'name': mobile['name'], 'prob': 0.4}],
                mobile['name']: [{'name': pc['name'], 'prob': 0.6}, {'name': calculator['name'], 'prob': 0.4}],
                calculator['name']: [{'name': pc['name'], 'prob': 0.1}, {'name': mobile['name'], 'prob': 0.2}, {'name': calculator['name'], 'prob': 0.1}, {'name': notebook['name'], 'prob': 0.6}],
                notebook['name']: [{'name': mobile['name'], 'prob': 0.1}, {'name': calculator['name'], 'prob': 0.4}, {'name': notebook['name'], 'prob': 0.5}]
            }}
        self.dealerB = {
            'name': dealerBName,
            'decription': 'risk averse, low price products',
            'index': 1,
            'stateTransition': {
                finalState['name']: [],
                laptop['name']: [],
                pc['name']: [],
                mobile['name']: [],
                calculator['name']: [{'name': mobile['name'], 'prob': 0.8}, {'name': calculator['name'], 'prob': 0.2}],
                notebook['name']: [{'name': calculator['name'], 'prob': 0.6}, {'name': notebook['name'], 'prob': 0.4}]
            }}
        self.dealerC = {
            'name': dealerCName,
            'decription': 'risk averse, high price products',
            'index': 2,
            'stateTransition': {
                finalState['name']: [],
                laptop['name']: [{'name': finalState['name'], 'prob': 0.2}, {'name': pc['name'], 'prob': 0.8}],
                pc['name']: [{'name': laptop['name'], 'prob': 0.7}, {'name': mobile['name'], 'prob': 0.3}],
                mobile['name']: [{'name': pc['name'], 'prob': 0.5}, {'name': mobile['name'], 'prob': 0.5}],
                calculator['name']: [],
                notebook['name']: []
            }}

class ActionFunctions:
    def __init__(self, actions, customActions):
        self.dealerA = copy.deepcopy(actions.dealerA)
        self.dealerB = copy.deepcopy(actions.dealerB)
        self.dealerC = copy.deepcopy(actions.dealerC)

        self.dealerA['stateTransition'] = self.createTailoredAction(self.dealerA, customActions)
        self.dealerB['stateTransition'] = self.createTailoredAction(self.dealerB, customActions)
        self.dealerC['stateTransition'] = self.createTailoredAction(self.dealerC, customActions)

        self.all = [self.dealerA,self.dealerB,self.dealerC]
        #if customRewards is not None and state['name'] in customRewards:
        # {'action':'A': [{'state': 'Mobile', 'transitions': [{'name': 'final', 'prob': 0.3}, {'name': 'pc', 'prob': 0.7}}] }

    def createTailoredAction(self, dealer, customActions):
        transitions = copy.deepcopy(dealer['stateTransition'])
        if customActions is not None and dealer['name'] in customActions:

            # [{'state': 'Mobile', 'transitions': [{'name': 'final', 'prob': 0.3}, {'name': 'pc', 'prob': 0.7}}]
            for stateTransition in customActions[dealer['name']]:
                transitions[stateTransition['state']] = stateTransition['transitions']

        return transitions

def getActions(states, customActions):
    return ActionFunctions(DefaultActions(states), customActions)

def getEfficiencyActions(states, customActions):
    return ActionFunctions(EfficiencyActions(states), customActions)

"""
from tradeState import getStates
import pprint
newActions = {'A': [{'state': 'mobile', 'transitions': [{'name': 'final', 'prob': 0.3}, {'name': 'pc', 'prob': 0.7}] }]}
ac2 = {'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}
#act = Actions(getStates(None))
pprint.pprint(getActions(getStates(None), ac2).dealerC)
pprint.pprint(getActions(getStates(None), ac2).dealerB)
#pprint.pprint(getActions(getStates(None), None).dealerA)
"""
