import os
import json
import ast
from pprint import pprint
import numpy as np
from tradeService import trainQTableForEpic
from tradeState import getStates, getEfficiencyStates
from tradeAction import getActions

class TradeEnvironment:
    def __init__(self, epicCount, updateQTable, filePath, alpha, gamma, exploitationProb, repeatCount, randomInitState, stateRewards, stateActions):
        self.epicCount = epicCount
        self.updateQTable = updateQTable
        self.filePath = filePath
        self.alpha = alpha
        self.gamma = gamma
        self.exploitationProb = exploitationProb
        self.repeatCount = repeatCount
        self.randomInitState = randomInitState
        self.stateRewards = stateRewards
        self.stateActions = stateActions

        customRewards = None
        if stateRewards is not None:
            customRewards = ast.literal_eval(stateRewards)
        self.states = getStates(customRewards)

        customActions = None
        if stateActions is not None:
            customActions = ast.literal_eval(stateActions)

        self.actions = getActions(self.states, customActions)

    # this could be replaced by method with proper name
    def __call__(self):
        createdFiles = []
        for repeatCnt in range(1,self.repeatCount + 1):
            fileName = self.filePath + str(repeatCnt) + '.json'
            if os.path.isfile(fileName) and self.updateQTable == True:
                with open(fileName) as data_file:
                    fileStr = data_file.read()
                    qTable = ast.literal_eval(fileStr)
                    self.findQVals(qTable, self.actions, self.states, fileName)
            else:
                qTable = [[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]]
                self.findQVals(qTable, self.actions, self.states, fileName)
                createdFiles.append(fileName)

        return createdFiles

    def findQVals(self, qTable, actions, states, fileName):
        def getInitState():
            if(self.randomInitState == True):
                return np.random.choice(states.nonFinalStates)
            else:
                return states.notebook
        # One epic is search from initial state to target state.
        for epicNr in range(self.epicCount):
            qTable = trainQTableForEpic(qTable, getInitState(), states, actions, self.exploitationProb, self.alpha, self.gamma)

        with open(fileName,"w") as patternWriter:
            jsonStr = json.dumps(qTable, indent=2, sort_keys=True)
            patternWriter.write(jsonStr)
