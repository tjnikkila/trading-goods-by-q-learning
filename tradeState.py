import copy
from tradeAction import dealerAName

class DefaultStates:
    finalState = {'name': 'final', 'reward': 100, 'index': 0}
    laptop = {'name': 'laptop', 'reward': 0, 'index': 1}
    pc = {'name': 'pc', 'reward': 0, 'index': 2}
    mobile = {'name': 'mobile', 'reward': 0, 'index': 3}
    calculator = {'name': 'calculator', 'reward': 0, 'index': 4}
    notebook = {'name': 'notebook', 'reward': 0, 'index': 5}

    states = [finalState, laptop, pc, mobile, calculator, notebook]

class EfficiencyStates:
    finalState = {'name': 'final', 'reward': 100, 'index': 0, 'bestAction': dealerAName}
    laptop = {'name': 'laptop', 'reward': 0, 'index': 1, 'bestAction': dealerAName}
    pc = {'name': 'pc', 'reward': 0, 'index': 2, 'bestAction': dealerAName}
    mobile = {'name': 'mobile', 'reward': 0, 'index': 3, 'bestAction': dealerAName}
    calculator = {'name': 'calculator', 'reward': 0, 'index': 4, 'bestAction': dealerAName}
    notebook = {'name': 'notebook', 'reward': 0, 'index': 5, 'bestAction': dealerAName}

    states = [finalState, laptop, pc, mobile, calculator, notebook]

class StateFunctions:

    def __init__(self, customRewards, states):
        self.customRewards = customRewards
        self.finalState = self.createTailoredState(states.finalState, customRewards)
        self.laptop = self.createTailoredState(states.laptop, customRewards)
        self.pc = self.createTailoredState(states.pc, customRewards)
        self.mobile = self.createTailoredState(states.mobile, customRewards)
        self.calculator = self.createTailoredState(states.calculator, customRewards)
        self.notebook = self.createTailoredState(states.notebook, customRewards)

        self.states = states.states
        self.all = [self.finalState, self.laptop, self.pc, self.mobile, self.calculator, self.notebook]
        self.nonFinalStates = self.all[1:]

    def createTailoredState(self, state, customRewards):
        if customRewards is not None and state['name'] in customRewards:
            customState = copy.deepcopy(state)
            customState['reward'] = customRewards[customState['name']]
            return customState
        else:
            return state

def getStates(customRewards):
    return StateFunctions(customRewards, DefaultStates())

def getEfficiencyStates(customRewards):
    return StateFunctions(customRewards, EfficiencyStates())
