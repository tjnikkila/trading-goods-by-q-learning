import argparse
import numpy as np
import time
from efficientTradeEnvironment import EfficientTradeEnvironment

parser = argparse.ArgumentParser()
parser.add_argument("--alpha", help="Value for alpha parameter in formula \"Q'[s, a] = (1 - α) · Q[s, a] + α · (r + γ · Q[s', argmaxa'(Q[s', a'])])\"",
                    type=float, required=False, default=0.2)
parser.add_argument("--gamma", help="Value for gamma parameter in formula \"Q'[s, a] = (1 - α) · Q[s, a] + α · (r + γ · Q[s', argmaxa'(Q[s', a'])])\"",
                    type=float, required=False, default=0.2)
parser.add_argument("--exploitationProb", help="Propability for exloitation. Values should be bertween 0 and 1. Probability for exploration is 1 - exploitationProb.",
                    type=float, required=False, default=0.67)
parser.add_argument("--repeatCount", help="How many times to repeat with same settings. Result files indexing is growing from one.",
                    type=int, required=False, default=1)

args = parser.parse_args()
alpha = args.alpha
gamma = args.gamma
exploitationProb = args.exploitationProb
repeatCount = args.repeatCount

averageEpics = np.array([])
start = time.time()
for cnt in range(0,repeatCount):
    averageEpic = EfficientTradeEnvironment().averageEpicsForTraining(alpha, gamma, exploitationProb, 100)
    print("Average epic " + str(averageEpic))
    averageEpics = np.append(averageEpics, averageEpic)
end = time.time()

print("Mean epic value over " + str(repeatCount) + " epics: " + str(averageEpics.mean()))
print("Standart deviation over " + str(repeatCount) + " epics: " + str(averageEpics.std()))
print("Time: " + str(end - start))
