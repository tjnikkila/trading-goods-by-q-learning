### How to Calculate Stability

There needs to be exact method to calculate stability in order to optimize it. Here stability is measured as follow:

1. Have all Q-table results in 2-dimensional matrices. Eg. result ```Q1 = [[1,2],[3,4]]``` and ```Q2 = [[1,3],[2,4]]```
2. Combine results so that all counterpart state-action -cells for one vector.  Eg. ```[[1,1], [2,3],[3,2],[4,4]]```
3. Normalize counterpart vectors. State transition from Laptop to reward would dominate if not normalized.
Eg ```[[1,1],[0.8,1.2],[1.2,0.8],[1,1]]```
4. Calculate standard deviation for each normalized rows. Eg ```[0.0, 0.2, 0.2, 0.0]```
5. Sum all standard deviations. Eg ```0.4```.
6. Divide sum by number of results to get average standard deviation per result. Eg. ```0.2```

[Script to calculate deviation](analysis/resultStd.py)

**Reference deviation is done by initial settings. Deviation is 1.1.**
