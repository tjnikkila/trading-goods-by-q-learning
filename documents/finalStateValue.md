
### Smaller and Bigger Final State

#### Final State a Has Small Reward

Final state is set to have reward 10 instead of 100

To Repeat, run:
```
python3 runTestSet.py --runName smallFinal --stateRewards "{'final': 10}"
```

Results:
```
python3 trade.py --filePath ./analysis/smallFinal/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9 --stateRewards {'final': 10}
```
Deviation 0.0464275795058
```
python3 trade.py --filePath ./analysis/smallFinal/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9 --stateRewards {'final': 10}
```
Deviation 0.0953658839727
```
python3 trade.py --filePath ./analysis/smallFinal/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards {'final': 10}
```
Deviation 0.0949469869097
```
python3 trade.py --filePath ./analysis/smallFinal/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards {'final': 10}
```
Deviation 0.0208325420978
```
python3 trade.py --filePath ./analysis/smallFinal/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9 --stateRewards {'final': 10}
```
Deviation 0.0357992985495
```
python3 trade.py --filePath ./analysis/smallFinal/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards {'final': 10}
```
Deviation 0.0456901489026
```
python3 trade.py --filePath ./analysis/smallFinal/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards {'final': 10}
```
Deviation 0.0454115460328
```
python3 trade.py --filePath ./analysis/smallFinal/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateRewards {'final': 10}
```
Deviation 0.0378565561327

#### Final State a Has Bigger Reward

Final state is set to have reward 200 instead of 100

To Repeat, run:
```
python3 runTestSet.py --runName bigFinal --stateRewards "{'final': 200}"
```

Results:
```
python3 trade.py --filePath ./analysis/bigFinal/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9 --stateRewards {'final': 200}
```
Deviation 0.0392897233589
```
python3 trade.py --filePath ./analysis/bigFinal/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9 --stateRewards {'final': 200}
```
Deviation 0.095881368755
```
python3 trade.py --filePath ./analysis/bigFinal/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards {'final': 200}
```
Deviation 0.0692058221785
```
python3 trade.py --filePath ./analysis/bigFinal/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards {'final': 200}
```
Deviation 0.0228950906241
```
python3 trade.py --filePath ./analysis/bigFinal/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9 --stateRewards {'final': 200}
```
Deviation 0.0332049147007
```
python3 trade.py --filePath ./analysis/bigFinal/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards {'final': 200}
```
Deviation 0.0497346672192
```
python3 trade.py --filePath ./analysis/bigFinal/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards {'final': 200}
```
Deviation 0.047909756205
```
python3 trade.py --filePath ./analysis/bigFinal/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateRewards {'final': 200}
```
Deviation 0.049834104741
#### Conclusion
Final state reward effects linearly. Doubling reward doubles Q-values. This was the only finding.
