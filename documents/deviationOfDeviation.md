### Calculating Deviation of Deviation

#### Mean and Average Deviation

Mean and average deviation is calculated as follow:

Deviations are calculated by running a [script](analysis/deviationOfDeviation.py)

```
# values of case qtable40k3
values = [0.111481606947, 0.0555899587928, 0.139803828284, 0.0505039882435, 0.0683966312213]
mean = sum(values) / len(values)
averageDeviation = sum([a for a in map(lambda a: abs(a - mean), values)])/len(values)
```

#### Measurements

```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k30--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 3
```

Deviation 0.111481606947
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k31--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0555899587928
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k32--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.139803828284
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k33--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0505039882435
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k34--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0683966312213

mean = 0.08515520269772,
averageDeviation = 0.03239001193422401
Precision: 0.09 ± 0.033

```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k30--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0752886825719
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k31--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0798308273865
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k32--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.124531199884
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k33--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.113783017393
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k34--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.120930919192

mean = 0.10287292928548002,
averageDeviation = 0.020250539445023995,
Precision: 0.10 ± 0.021,

```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k30--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.102810863361
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k31--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0779371832055
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k32--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0627731026719
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k33--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.10502383753
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k34--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0871341968654

mean = 0.08713583672676,
averageDeviation = 0.013425210974991997,
Precision: 0.09 ± 0.014
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k30--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.104091375709
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k31--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.066371547428
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k32--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.090048240016
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k33--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0723669421271
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k34--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 3
```
Deviation 0.0720789018374

mean = 0.0809914014235,
averageDeviation = 0.012862725151199999,
Precision: 0.08 ± 0.013
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k50--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0504965220794
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k51--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0742593623212
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k52--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0705267778825
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k53--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0583962628489
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k54--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0614287559139

mean = 0.06302153620918001,
averageDeviation = 0.007497227114136003,
Precision: 0.06 ± 0.008


```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k50--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0735574912907
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k51--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0484746894561
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k52--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0625199119269
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k53--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0662592656072
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k54--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0813049222897

mean = 0.06642325611411999,
averageDeviation = 0.008806360540863999,
Precision: 0.07 ± 0.009

```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k50--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0669239750637
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k51--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0645821606199
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k52--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0471923519562
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k53--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0530049653932
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k54--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0715904027096

mean = 0.060658771148519995,
averageDeviation = 0.008448089979056002,
Precision: 0.06 ± 0.009

```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k50--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0567124270648
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k51--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.068182293466
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k52--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0690893585011
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k53--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0570074094143
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k54--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0698397602228

mean = 0.0641662497338,
averageDeviation = 0.005845065195399998,
Precision: 0.06 ± 0.006

```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k70--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0423989793288
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k71--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0463289030458
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k72--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0385785066093
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k73--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0479552569078
```
python3 trade.py --filePath ./analysis/testSetSize/qtable40k74--epicCount 40000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0468613212466

mean = 0.04442459342766,
averageDeviation = 0.003148680366888,
Precision: 0.04 ± 0.004

```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k70--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.049920018356
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k71--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0452988113554
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k72--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0418556750976
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k73--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0388833360444
```
python3 trade.py --filePath ./analysis/testSetSize/qtable50k74--epicCount 50000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0462453884844

mean = 0.04444064586756,
averageDeviation = 0.0032569122372479997,
Precision: 0.04 ± 0.004

```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k70--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.050476740258
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k71--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0387900728571
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k72--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0531832668625
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k73--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0538772403198
```
python3 trade.py --filePath ./analysis/testSetSize/qtable80k74--epicCount 80000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0522432656054

mean = 0.04971411718056,
averageDeviation = 0.004369617729384001,
Precision: 0.05 ± 0.005

```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k70--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0568339055826
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k71--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0495785706783
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k72--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0501900070245
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k73--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0360432922188
```
python3 trade.py --filePath ./analysis/testSetSize/qtable100k74--epicCount 100000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0498426310209

mean = 0.048497681305019996,
averageDeviation = 0.004981755634488004,
Precision: 0.05 ± 0.005

```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k50--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0620082155299
```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k51--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0705476443724
```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k52--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0565913821915
```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k53--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.059944804509
```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k54--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0602528375976

mean = 0.061868976840080005,
averageDeviation = 0.003527162488856002,
Precision: 0.06 ± 0.004

```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k70--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0503476462399
```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k71--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.050083313129
```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k72--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0468383297698
```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k73--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0430393655138
```
python3 trade.py --filePath ./analysis/testSetSize/qtable30k74--epicCount 30000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0506313074078

mean = 0.04818799241206,
averageDeviation = 0.0025993158162080005,
Precision: 0.05 ± 0.003

```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k50--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0518499435238
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k51--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0671280649618
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k52--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0528591523877
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k53--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0739136944908
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k54--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0514387092008

mean = 0.05943791291298001,
averageDeviation = 0.008866373450656002,
Precision: 0.06 ± 0.009

```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k70--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0422415042963
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k71--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0417123169258
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k72--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0455994671155
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k73--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0517411005128
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k74--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0376747442418

mean = 0.04379382661844,
averageDeviation = 0.003901165756568002,
Precision: 0.04 ± 0.004

```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k50--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0549874991723
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k51--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0725281860437
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k52--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0778782884347
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k53--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0724395091702
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k54--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0680857947721

mean = 0.0691838555186,
averageDeviation = 0.00611776683712,
Precision: 0.07 ± 0.007

```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k70--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.042772523816
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k71--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0512909948907
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k72--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0488548480938
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k73--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.064272016903
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k74--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0673250311763

mean = 0.05490308297595999,
averageDeviation = 0.008716352850951996,
Precision: 0.05 ± 0.009

```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k50--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.103843975188
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k51--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.207982627638
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k52--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.120956131874
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k53--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.120457276032
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k54--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 5
```
Deviation 0.0890710975048

mean = 0.12846222164736001,
averageDeviation = 0.03180816239625601,
Precision: 0.13 ± 0.032

```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k70--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.127544547376
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k71--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0819662128544
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k72--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0824839509362
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k73--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.0848375735573
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k74--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 7
```
Deviation 0.132252321622

mean = 0.10181692126918,
averageDeviation = 0.022465210583855998,
Precision: 0.10 ± 0.023


```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k90--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0422772388294
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k91--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0371705252019
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k92--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0363083013341
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k93--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0314939783673
```
python3 trade.py --filePath ./analysis/testSetSize/qtable20k94--epicCount 20000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0383488337687

mean = 0.03711977550028,
averageDeviation = 0.002574908519664,
Precision: 0.04 ± 0.003

```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k90--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.041569352039
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k91--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.029033140444
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k92--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0375401829839
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k93--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0382762685705
```
python3 trade.py --filePath ./analysis/testSetSize/qtable10k94--epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0294326683722

mean = 0.03517032248192,
averageDeviation = 0.004749934459056001,
Precision: 0.04 ± 0.005

```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k90--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0585183569639
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k91--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0643795186546
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k92--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0667284659492
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k93--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0968348610532
```
python3 trade.py --filePath ./analysis/testSetSize/qtable5k94--epicCount 5000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0484444336493

mean = 0.06698112725404,
averageDeviation = 0.011941493519664,
Precision: 0.07 ± 0.012

It seems like epicCount 10000 and repeatCount 9 gives one of the best results and is lightest to calculate among bests.
