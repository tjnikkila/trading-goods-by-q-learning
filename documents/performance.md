## Model Accuracy vs Performance

Model in the example is very simple. It does not require a lot of resources to train Q-values for the model. However real world models are much more
complicated and they require lot more resources to train Q-values. Question is, when Q-values are good enough to make reasonable decisions
and how to optimize parameters to use resources efficiently?

There are 6 states in our simple model. Lets do the settings so that for each state there is one good action to take and one bad action to take. Good action is
taking actor towards reward with probability 0.7 and staying in the same state by probability 0.3. Bad action is the opposite.
Starting state is notebook and ending state is final state. Two measures are measured during training. First how many epics of training is needed before
Q-values are suggesting to take only good actions. Second how much time it took to train until first epic of only good actions. Simulation is run on one processor. Learning rate and propagation rate and exploitation rate are variables.

For each case epics count to pass test is calculated 100 times and average epics to pass is treated as one value. 10 of values are calculated. Following measurements are calculated over these 10 values: mean, standard deviation and time consumption(one processor).

|α    |γ       |exloitation probability|mean   |std   |time |command|
|-----|--------|-----------------------|-------|------|-----|-------|
|0.2  |0.2     |0.67                   |12.59  |0.56  |6.0  |[1]    |
|0.02 |0.2     |0.67                   |51.40  |2.11  |25.6 |[2]    |
|0.001|0.2     |0.67                   |1225.79|115.04|571.0|[3]    |
|0.4  |0.2     |0.67                   |10.44  |0.32  |5.4  |[4]    |
|0.6  |0.2     |0.67                   |10.02  |0.31  |4.9  |[5]    |
|0.8  |0.2     |0.67                   |9.85   |0.32  |4.6  |[6]    |
|0.9  |0.2     |0.67                   |9.91   |0.40  |5.4  |[7]    |
|0.93 |0.2     |0.67                   |9.73   |0.36  |4.7  |[8]    |
|0.96 |0.2     |0.67                   |9.89   |0.32  |4.6  |[9]    |
|0.99 |0.2     |0.67                   |10.04  |0.23  |4.8  |[10]   |
|1.0  |0.2     |0.67                   |12.18  |0.47  |6.0  |[11]   |
|0.2  |0.02    |0.67                   |11.60  |0.27  |5.8  |[12]   |
|0.2  |0.002   |0.67                   |11.74  |0.44  |5.5  |[13]   |
|0.2  |0.000001|0.67                   |11.79  |0.64  |5.6  |[14]   |
|0.2  |0.3     |0.67                   |12.42  |0.70  |6.1  |[15]   |
|0.2  |0.5     |0.67                   |13.96  |0.54  |7.3  |[16]   |
|0.2  |0.7     |0.67                   |16.15  |0.79  |8.3  |[17]   |
|0.2  |0.2     |0.9                    |28.60  |1.03  |14.7 |[18]   |
|0.2  |0.2     |0.5                    |9.5    |0.29  |4.9  |[19]   |
|0.2  |0.2     |0.3                    |8.4    |0.31  |4.2  |[20]   |
|0.2  |0.2     |0.1                    |7.94   |0.23  |3.9  |[21]   |
|0.2  |0.2     |0.01                   |8.06   |0.37  |3.9  |[22]   |
|0.2  |0.2     |0.001                  |7.86   |0.23  |3.9  |[23]   |
|0.2  |0.2     |0.00001                |7.98   |0.27  |3.8  |[24]   |

**conclusion**: small alpha is horrible for performance. It does not matter how small
gamma is as long as it is not zero. Model starts to break if gamma is greater than 0.5.
Performance drops if exploitation probability is greater than 0.5. Exploitation rate can be
almost zero without impact to performance. Maybe gamma and exploitation rate can be almost zero because model is so simple.

Lesson learned:
* In real world situations alpha should be greater than 0.1. 
* Gamma should not be bigger than 0.7
* exploitation rate should not be much bigger than 0.5 when model is trained.

Deviation with final parameters:
```
python3 trade.py --filePath ./analysis/qtableFinalParams --alpha 0.1 --exploitationProb 0.5 --epicCount 10000  --repeatCount 9
```

Deviation 0.68108964846304265

Commands:
[1] python3 efficientTrade.py --repeatCount 10

[2] python3 efficientTrade.py --repeatCount 10 --alpha 0.02

[3] python3 efficientTrade.py --repeatCount 10 --alpha 0.001

[4] python3 efficientTrade.py --repeatCount 10 --alpha 0.4

[5] python3 efficientTrade.py --repeatCount 10 --alpha 0.6

[6] python3 efficientTrade.py --repeatCount 10 --alpha 0.8

[7] python3 efficientTrade.py --repeatCount 10 --alpha 0.9

[8] python3 efficientTrade.py --repeatCount 10 --alpha 0.93

[9] python3 efficientTrade.py --repeatCount 10 --alpha 0.96

[10] python3 efficientTrade.py --repeatCount 10 --alpha 0.99

[11] python3 efficientTrade.py --repeatCount 10 --alpha 1.0

[12] python3 efficientTrade.py --repeatCount 10 --gamma 0.02

[13] python3 efficientTrade.py --repeatCount 10 --gamma 0.002

[14] python3 efficientTrade.py --repeatCount 10 --gamma 0.000001

[15] python3 efficientTrade.py --repeatCount 10 --gamma 0.3

[16] python3 efficientTrade.py --repeatCount 10 --gamma 0.5

[17] python3 efficientTrade.py --repeatCount 10 --gamma 0.7

[18] python3 efficientTrade.py --repeatCount 10 --exploitationProb 0.9

[19] python3 efficientTrade.py --repeatCount 10 --exploitationProb 0.5

[20] python3 efficientTrade.py --repeatCount 10 --exploitationProb 0.3

[21] python3 efficientTrade.py --repeatCount 10 --exploitationProb 0.1

[22] python3 efficientTrade.py --repeatCount 10 --exploitationProb 0.01

[23] python3 efficientTrade.py --repeatCount 10 --exploitationProb 0.001

[24] python3 efficientTrade.py --repeatCount 10 --exploitationProb 0.00001

