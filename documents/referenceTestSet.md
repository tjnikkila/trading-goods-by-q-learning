
### Reference Test Set

This test set is used as reference deviance for the rest of the tests in this section.

To Repeat, run:
```
python3 runTestSet.py --runName reference
```
Results:
```
python3 trade.py --filePath ./analysis/reference/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9
```
Deviation 0.0416323501568
```
python3 trade.py --filePath ./analysis/reference/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9
```
Deviation 0.0981349589689
```
python3 trade.py --filePath ./analysis/reference/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9
```
Deviation 0.0811652102787
```
python3 trade.py --filePath ./analysis/reference/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9
```
Deviation 0.0243330318105
```
python3 trade.py --filePath ./analysis/reference/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9
```
Deviation 0.0331826241479
```
python3 trade.py --filePath ./analysis/reference/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9
```
Deviation 0.0471773193167
```
python3 trade.py --filePath ./analysis/reference/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9
```
Deviation 0.0406117600311
```
python3 trade.py --filePath ./analysis/reference/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9
```
Deviation 0.0649321268649
