### Two actor per state. How distribution of action probabilities effect to Q-learning parameters

**One big probability and one small probabilities**

Absolute Q-values are not comparable.

To repeat, run:
```
python3 runTestSet.py --runName bigAndSmallProb --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Results:
```
python3 trade.py --filePath ./analysis/bigAndSmallProb/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Deviation 0.0400155082187
```
python3 trade.py --filePath ./analysis/bigAndSmallProb/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Deviation 0.0813752947281
```
python3 trade.py --filePath ./analysis/bigAndSmallProb/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Deviation 0.110539242982
```
python3 trade.py --filePath ./analysis/bigAndSmallProb/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Deviation 0.0221153389125
```
python3 trade.py --filePath ./analysis/bigAndSmallProb/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Deviation 0.0458906030972
```
python3 trade.py --filePath ./analysis/bigAndSmallProb/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Deviation 0.0484799069618
```
python3 trade.py --filePath ./analysis/bigAndSmallProb/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Deviation 0.0452436261854
```
python3 trade.py --filePath ./analysis/bigAndSmallProb/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.9}, {'name': 'mobile', 'prob': 0.1}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.1}, {'name': 'mobile', 'prob': 0.9}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.9}, {'name': 'calculator', 'prob': 0.1}]}]}"
```
Deviation 0.12654739434

**Two equal probabilities**

To Repeat, run:
```
python3 runTestSet.py --runName twoEqualProbs --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Results:
```
python3 trade.py --filePath ./analysis/twoEqualProbs/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Deviation 0.109803962939
```
python3 trade.py --filePath ./analysis/twoEqualProbs/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Deviation 0.0501811246944
```
python3 trade.py --filePath ./analysis/twoEqualProbs/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Deviation 0.224784079699
```
python3 trade.py --filePath ./analysis/twoEqualProbs/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Deviation 0.0674344952189
```
python3 trade.py --filePath ./analysis/twoEqualProbs/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Deviation 0.105886812354
```
python3 trade.py --filePath ./analysis/twoEqualProbs/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Deviation 0.113296444122
```
python3 trade.py --filePath ./analysis/twoEqualProbs/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Deviation 0.0386259133993
```
python3 trade.py --filePath ./analysis/twoEqualProbs/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateActions "{'C': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}], 'A': [{'state': 'pc', 'transitions': [{'name': 'final', 'prob': 0.5}, {'name': 'mobile', 'prob': 0.5}]}, {'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}] }], 'B': [{'state': 'calculator', 'transitions':  [{'name': 'mobile', 'prob': 0.5}, {'name': 'calculator', 'prob': 0.5}]}]}"
```
Deviation 0.113980327576

**conclusion**: Seems like equal probabilities requires smaller learning rate and greater epics count to have the same deviation. In other words equal probabilities are harder to learn.
