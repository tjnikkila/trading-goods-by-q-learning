
### Three actions. Does deviance increase and how it behaves with learning rate

To Repeat, run:
```
python3 runTestSet.py --runName threeActions --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Results:
```
python3 trade.py --filePath ./analysis/threeActions/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.0442756027609, 0.0585962428527
```
python3 trade.py --filePath ./analysis/threeActions/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.0933177175415, 0.0844945738973
```
python3 trade.py --filePath ./analysis/threeActions/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.0806744307486, 0.122995596922
```
python3 trade.py --filePath ./analysis/threeActions/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.0267833233908, 0.0219331693792
```
python3 trade.py --filePath ./analysis/threeActions/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.0712133051087, 0.0583922218944
```
python3 trade.py --filePath ./analysis/threeActions/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.0429565135938, 0.0370533023619
```
python3 trade.py --filePath ./analysis/threeActions/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.0446970395335, 0.0521934966058
```
python3 trade.py --filePath ./analysis/threeActions/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.105598899499, 0.0959678004872

**Conclusion**: Seems as only difference between reference and this test case is how deviation responds to gamma. If gamma gets bigger, then deviation increases in a case of more actions to choose from. Based on earlier tests, I suspect that smaller gamma is not making model any better. What would happen if gamma gets bigger and more epics are run? Is Deviation coming down?

To repeat, run:
```
python3 trade.py --filePath ./analysis/threeActions/qtableGamma05epics40k --epicCount 40000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateActions "{'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}"
```
Deviation 0.045020787350270944

**conclusion**: Deviation comes down in a case of bigger gamma, when more epics are run. All in all it seems like more actions, harder it is to learn.
