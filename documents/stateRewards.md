
### State with reward

#### Mobile State Has Small Reward

Reward of 10 added to mobile state. State is quite in the middle of graph.

To Repeat, run:
```
python3 runTestSet.py --runName mobile10 --stateRewards "{'mobile': 10}"
```

Results:

```
python3 trade.py --filePath ./analysis/mobile10/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9 --stateRewards "{'mobile': 10}"
```
Deviation 0.0292483513441
```
python3 trade.py --filePath ./analysis/mobile10/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9 --stateRewards "{'mobile': 10}"
```
Deviation 0.0593672102426
```
python3 trade.py --filePath ./analysis/mobile10/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards "{'mobile': 10}"
```
Deviation 0.0389308333508
```
python3 trade.py --filePath ./analysis/mobile10/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards "{'mobile': 10}"
```
Deviation 0.017975286154
```
python3 trade.py --filePath ./analysis/mobile10/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9 --stateRewards "{'mobile': 10}"
```
Deviation 0.0572244023773
```
python3 trade.py --filePath ./analysis/mobile10/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards "{'mobile': 10}"
```
Deviation 0.0318742784119
```
python3 trade.py --filePath ./analysis/mobile10/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards "{'mobile': 10}"
```
Deviation 0.0257592524227
```
python3 trade.py --filePath ./analysis/mobile10/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateRewards "{'mobile': 10}"
```
Deviation 0.0575995080722

Conclusion: Absolute value of  almost every Q-value increased. In some files of qtableAlpha005 and qtableGamma005epics40k laptop state Q-values did not increase. Seems like all Q-values in qtableGamma0051 increased, even if laptop state was very close. Q-values of other than laptop state increased always and significantly. So adding more reward to states, do increase Q-values. This seems pretty obvious. It is not so obvious why DealerA is not relatively as good anymore compared to DealerB and DealerC. DealerA is losing in states mobile and calculator in every repeat. This must be because of DealerA is unlike to go to state mobile. From notebook the probability is 0.1 and from calculator the probability is 0.2. DealerB goes from state calculator to state mobile with probability 0.8, which is a lot. DealerC goes from state mobile to state mobile with probability 0.5, which is rather big. If state notebook had reward, then DealerA must be doing much better compared to DealerB.

Adding reward to more states may have reduced amount of deviation slightly. Theory: Closer the state is to reward, less it's Q-value is deviating. To test: if reward is not for mobile, but for laptop, then there would be more deviation.

First time I tried this it seemed like bigger gamma value decreases relative difference of Q-values between dealers. Absolute difference stays roughly the same. When running experiment again with bit different settings, I did not find this pattern anymore. What can be learned is not to make too subtle conclusion until repeated few times. Sometimes it is just luck.

#### Notebook State Has Small Reward

Reward of 10 added to notebook state. State is the furthest state from reward. Theory from *Mobile State Has Small Reward* is that DealerA should do much better compared to DealerB when notebook state has reward instead of mobile state.

To Repeat, run:
```
python3 runTestSet.py --runName notebook10 --stateRewards "{'notebook': 10}"
```
Results:
```
python3 trade.py --filePath ./analysis/notebook10/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9 --stateRewards "{'notebook': 10}"
```
Deviation 0.032435495041
```
python3 trade.py --filePath ./analysis/notebook10/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9 --stateRewards "{'notebook': 10}"
```
Deviation 0.0544707866672
```
python3 trade.py --filePath ./analysis/notebook10/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards "{'notebook': 10}"
```
Deviation 0.0541438581368
```
python3 trade.py --filePath ./analysis/notebook10/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards "{'notebook': 10}"
```
Deviation 0.0163778685432
```
python3 trade.py --filePath ./analysis/notebook10/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9 --stateRewards "{'notebook': 10}"
```
Deviation 0.0258273602735
```
python3 trade.py --filePath ./analysis/notebook10/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards "{'notebook': 10}"
```
Deviation 0.0325707986503
```
python3 trade.py --filePath ./analysis/notebook10/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards "{'notebook': 10}"
```
Deviation 0.0355384141598
```
python3 trade.py --filePath ./analysis/notebook10/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateRewards "{'notebook': 10}"
```
Deviation 0.0197826364887

Average of 9 qtable files.
```
[[ 31.16981974,   0.        ,  21.33934542],
 [ 13.2882286 ,   0.        ,   4.4618848 ],
 [  2.18140747,   0.        ,   1.54290499],
 [  7.20235632,   0.63463386,   0.        ],
 [  6.33211871,   5.36306856,   0.        ]]
```

Conclusion: DealerA is winning in all states. DealerA is winning tenfold compared to DealerB on state calculator. When gamma is very small, like 0.005, DealerA is winning DealerB in state calculator by a factor of 100. So the theory was proven right.

Seems like small reward of 10 in state notebook does not effect to Q-values of the states laptop and pc almost at all. These are two furthest states from notebook and two closest states to final state with rewards 100. All this makes sense. Small reward on one end and big reward on another. Big reward is dominating.

Small reward seems to increase Q-value of DealerA a bit in state mobile. Q-value of DealerC seems to be almost the same as when there is no reward on state notebook. The first impression was that it is so because DealerA is possible action in any state, but DealerC is not active at states notebook and calculator. The intuition was that DealerA has direct link from notebook to mobile. However, naming one action of each state as DealerA is arbitrary. All DealerAs could be renamed to what ever and result would be the same. DealerAs are completely independent from one state to another. The only thing that matters in state mobile is that first action is connected to state calculator by probability of 0.2. State calculator is then connected to state notebook. This makes reward flow from notebook to Q-value of one action in state mobile. The second action in state mobile is connected to state mobile with probability of 0.5. From there it is possible to choose first action that is connected to state calculator. So path from second action of mobile to notebook is longer and less rewarding.

It is notable, that Q-values of state calculator, which is next to rewarded state notebook, gets the most of reward in state notebook. So adding reward to state, say notebook, does not increase Q-values of that state, but Q-values of what ever state is tightest connected to reward state.

#### Laptop State Has Small Reward

Reward of 10 added to laptop state. State is the closest state from final state. Theory from *Mobile State Has Small Reward* is that further away state is from reward, more there is deviation. Notebook state has longer distance to laptop state than mobile state. So if reward is moved from mobile state to laptop state, then deviation should increase.

To Repeat, run:
```
python3 runTestSet.py --runName laptop10 --stateRewards "{'laptop': 10}"
```
Results:
```
python3 trade.py --filePath ./analysis/laptop10/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount 9 --stateRewards "{'laptop': 10}"
```
Deviation 0.0314577286815
```
python3 trade.py --filePath ./analysis/laptop10/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount 9 --stateRewards "{'laptop': 10}"
```
Deviation 0.0724642481468
```
python3 trade.py --filePath ./analysis/laptop10/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards "{'laptop': 10}"
```
Deviation 0.0976316209715
```
python3 trade.py --filePath ./analysis/laptop10/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount 9 --stateRewards "{'laptop': 10}"
```
Deviation 0.0224146724024
```
python3 trade.py --filePath ./analysis/laptop10/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount 9 --stateRewards "{'laptop': 10}"
```
Deviation 0.038224779437
```
python3 trade.py --filePath ./analysis/laptop10/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards "{'laptop': 10}"
```
Deviation 0.0385258856941
```
python3 trade.py --filePath ./analysis/laptop10/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount 9 --stateRewards "{'laptop': 10}"
```
Deviation 0.0357020602561
```
python3 trade.py --filePath ./analysis/laptop10/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount 9 --stateRewards "{'laptop': 10}"
```
Deviation 0.0574479642173

Conclusion: Cannot say if deviation increased a bit or not when reward was moved from state mobile to state laptop. Seems like theory, further from reward greater the deviation, is not proved to be right.
