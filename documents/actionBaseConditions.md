Base test case is to run same α, γ and epics tests as with reward changes. For all action test cases, the same action setting is given to two states
to amplify the effect. Chosen states are pc and calculator. When it comes to add actions, DealerB gets DealerC actions and DealerC gets DealerB actions.
For example DealerB default stateTransitions would look like this

```
  finalState['name']: [],
  laptop['name']: [],
  pc['name']: [{'name': laptop['name'], 'prob': 0.7}, {'name': mobile['name'], 'prob': 0.3}],
  mobile['name']: [],
  calculator['name']: [{'name': mobile['name'], 'prob': 0.8}, {'name': calculator['name'], 'prob': 0.2}],
  notebook['name']: [{'name': calculator['name'], 'prob': 0.6}, {'name': notebook['name'], 'prob': 0.4}]
```
