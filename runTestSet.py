from tradeEnvironment import TradeEnvironment
from standardDeviation import TradeStd
import os
from pprint import pprint
import threading

import argparse

from tradeEnvironment import TradeEnvironment

asStr = lambda s: s or ""

def toParamParam(paramName, paramValue):
    if paramValue is None:
        return ""
    else:
        return " --" + paramName + " \"" + paramValue + "\""


class RunBaseCaseAndAlpha:
    def __init__(self,folderName, repeatCount):
        self.folderName = folderName
        self.repeatCount = repeatCount

    def __call__(self):
        std = TradeStd()
        qtableCreatedFiles = TradeEnvironment(10000, False, self.folderName +  "/qtable", 0.001, 0.2, 0.67, self.repeatCount, True, args.stateRewards, args.stateActions)()
        qtableDeviation = std(qtableCreatedFiles)
        alpha005CreatedFiles = TradeEnvironment(10000, False, self.folderName +  "/qtableAlpha005", 0.005, 0.2, 0.67, self.repeatCount, True, args.stateRewards, args.stateActions)()
        alpha005Deviation = std(alpha005CreatedFiles)
        alpha0004CreatedFiles = TradeEnvironment(10000, False, self.folderName +  "/qtableAlpha0004", 0.0004, 0.2, 0.67, self.repeatCount, True, args.stateRewards, args.stateActions)()
        alpha0004Deviation = std(alpha0004CreatedFiles)
        alpha0004epics40kCreatedFiles = TradeEnvironment(40000, False, self.folderName +  "/qtableAlpha0004epics40k", 0.0004, 0.2, 0.67, self.repeatCount, True, args.stateRewards, args.stateActions)()
        alpha0004epics40kDeviation = std(alpha0004epics40kCreatedFiles)

        rewardStr = toParamParam("stateRewards", args.stateRewards)
        actionStr = toParamParam("stateActions", args.stateActions)

        print("python3 trade.py --filePath ./" + self.folderName + "/qtable --epicCount 10000 --alpha 0.001 --randomInitState True --repeatCount " + str(self.repeatCount) + rewardStr + actionStr)
        print("Deviation " + str(qtableDeviation))
        print("python3 trade.py --filePath ./" + self.folderName + "/qtableAlpha005 --epicCount 10000 --alpha 0.005 --randomInitState True --repeatCount " + str(self.repeatCount) + rewardStr + actionStr)
        print("Deviation " + str(alpha005Deviation))
        print("python3 trade.py --filePath ./" + self.folderName + "/qtableAlpha0004 --epicCount 10000 --alpha 0.0004 --randomInitState True --repeatCount " + str(self.repeatCount) + rewardStr + actionStr)
        print("Deviation " + str(alpha0004Deviation))
        print("python3 trade.py --filePath ./" + self.folderName + "/qtableAlpha0004epics40k --epicCount 40000 --alpha 0.0004 --randomInitState True --repeatCount " + str(self.repeatCount) + rewardStr + actionStr)
        print("Deviation " + str(alpha0004epics40kDeviation))

class RunGammaTests:
    def __init__(self,folderName, repeatCount):
        self.folderName = folderName
        self.repeatCount = repeatCount

    def __call__(self):
        std = TradeStd()
        gamma03CreatedFiles = TradeEnvironment(10000, False, self.folderName +  "/qtableGamma03", 0.001, 0.3, 0.67, self.repeatCount, True, args.stateRewards, args.stateActions)()
        gamma03Deviation = std(gamma03CreatedFiles)
        gamma005CreatedFiles = TradeEnvironment(10000, False, self.folderName +  "/qtableGamma005", 0.001, 0.05, 0.67, self.repeatCount, True, args.stateRewards, args.stateActions)()
        gamma005Deviation = std(gamma005CreatedFiles)
        gamma005epics40kCreatedFiles = TradeEnvironment(40000, False, self.folderName +  "/qtableGamma005epics40k", 0.001, 0.05, 0.67, self.repeatCount, True, args.stateRewards, args.stateActions)()
        gamma005epics40kDeviation = std(gamma005epics40kCreatedFiles)
        gamma05CreatedFiles = TradeEnvironment(10000, False, self.folderName +  "/qtableGamma05", 0.001, 0.5, 0.67, self.repeatCount, True, args.stateRewards, args.stateActions)()
        gamma05Deviation = std(gamma05CreatedFiles)

        rewardStr = toParamParam("stateRewards", args.stateRewards)
        actionStr = toParamParam("stateActions", args.stateActions)

        print("python3 trade.py --filePath ./" + self.folderName + "/qtableGamma03 --epicCount 10000 --alpha 0.001 --gamma 0.3 --randomInitState True --repeatCount " + str(self.repeatCount) + rewardStr + actionStr)
        print("Deviation " + str(gamma03Deviation))
        print("python3 trade.py --filePath ./" + self.folderName + "/qtableGamma005 --epicCount 10000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount " + str(self.repeatCount) + rewardStr + actionStr)
        print("Deviation " + str(gamma005Deviation))
        print("python3 trade.py --filePath ./" + self.folderName + "/qtableGamma005epics40k --epicCount 40000 --alpha 0.001 --gamma 0.05 --randomInitState True --repeatCount " + str(self.repeatCount) + rewardStr + actionStr)
        print("Deviation " + str(gamma005epics40kDeviation))
        print("python3 trade.py --filePath ./" + self.folderName + "/qtableGamma05 --epicCount 10000 --alpha 0.001 --gamma 0.5 --randomInitState True --repeatCount " + str(self.repeatCount) + rewardStr + actionStr)
        print("Deviation " + str(gamma05Deviation))


parser = argparse.ArgumentParser()

parser.add_argument("--runName", help="Groups generated files based on run name. Run name becomes folder and files are saved in that folder under" \
+ "folder 'analysis'. If file base name is 'abc', runName is 'def', then first file is like './analysis/def/abc1.json'",
                    type=str, required=False, default='testSetRun')
parser.add_argument("--stateRewards", help="Original state rewards can be override by this parameter. Example: {\"mobile\": 10,\"pc\": 11}.",
                    type=str, required=False, default=None)
parser.add_argument("--stateActions", help="Original state actions can be override by this parameter. Example: [{'action':'dealerA': [{'state': 'Mobile', 'transitions': [{'name': 'final', 'prob': 0.3}, {'name': 'pc', 'prob': 0.7}}] }].",
                    type=str, required=False, default=None)

args = parser.parse_args()


 # python3 runTestSet.py --runName reference
folderName = "analysis/" + args.runName
os.makedirs(folderName, exist_ok=True)
repeatCount = 9
d1 = threading.Thread(target=RunBaseCaseAndAlpha(folderName, repeatCount))
d2 = threading.Thread(target=RunGammaTests(folderName, repeatCount))

d1.start()
d2.start()

d1.join()
d2.join()
