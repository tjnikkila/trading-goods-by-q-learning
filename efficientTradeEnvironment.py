from tradeService import validateSteps, trainQTableForEpic
from tradeState import getStates, getEfficiencyStates
from tradeAction import getEfficiencyActions

class EfficientTradeEnvironment:

    def averageEpicsForTraining(self, alpha, gamma, exploitationProb, averageOver):
        states = getEfficiencyStates(None)
        actions = getEfficiencyActions(states, None)
        totalEpics = 0
        for i in range(0,averageOver):
            passAllFirst = False
            qTable = [[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]]
            while passAllFirst == False:
                qTable = trainQTableForEpic(qTable, states.notebook, states, actions, exploitationProb, alpha, gamma)
                totalEpics += 1
                # go through qtable step by step to test if it always chose right action.
                if validateSteps(states.notebook, qTable, states, actions):
                    passAllFirst = True

        return totalEpics / averageOver

#EfficientTradeEnvironment(0.2,0.2,0.67)
