import os
import json
import ast
from pprint import pprint
import random
import numpy as np
import argparse

class TradeStd:
    def __call__(self, fileNames):
        files = fileNames
        results = []

        for fileName in files:
            with open('./' + fileName) as data_file:
                fileStr = data_file.read()
                qTable = ast.literal_eval(fileStr)
                results.append(qTable[1:])

        res = np.array(results)

        numberOfSamples = len(files)
        combinedStd = 0.0
        for stateIndex in range(0,5):
            for actorNr in range(0,3):
                counterparts = res[:,stateIndex,actorNr]
                normalized = np.nan_to_num(counterparts / np.mean(counterparts))
                combinedStd += normalized.std()
        return combinedStd/numberOfSamples
