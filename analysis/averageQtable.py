import os
import json
import ast
from pprint import pprint
import random
import numpy as np

baseFile = "notebook10/qtable"
files = [baseFile+'1.json', baseFile+'2.json', baseFile+'3.json', baseFile+'4.json',baseFile+'5.json', baseFile+'6.json', baseFile+'7.json', baseFile+'8.json', baseFile+'9.json']
results = []

for fileName in files:
    with open('./' + fileName) as data_file:
        fileStr = data_file.read()
        qTable = ast.literal_eval(fileStr)
        results.append(qTable[1:])

res = np.array(results)

pprint(np.nan_to_num(np.sum(res, axis=0) / len(res)))
