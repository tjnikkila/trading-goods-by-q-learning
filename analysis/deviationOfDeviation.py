from tradeEnvironment import TradeEnvironment
from standardDeviation import TradeStd
import os
from pprint import pprint

import argparse

from tradeEnvironment import TradeEnvironment
def runDeviationComparisonSet(baseFileName, epicCount, repeatCount, testRunCount, std):
    for loopCnt in range(testRunCount):
        qtableCreatedFiles = TradeEnvironment(epicCount, False, baseFileName + "_" + str(loopCnt), 0.001, 0.2, 0.67, repeatCount, True, None)()
        qtableDeviation = std(qtableCreatedFiles)
        print("python3 trade.py --filePath ./" + baseFileName  + str(loopCnt) + "--epicCount " + str(epicCount) + " --alpha 0.001 --randomInitState True --repeatCount " + str(repeatCount))
        print("Deviation " + str(qtableDeviation))


parser = argparse.ArgumentParser()

args = parser.parse_args()

folderName = "analysis/testSetSize"
os.makedirs(folderName, exist_ok=True)
repeatCount = 5
std = TradeStd()

runDeviationComparisonSet(folderName +  "/qtable40k3", 40000, 3, 5, std)
runDeviationComparisonSet(folderName +  "/qtable50k3", 50000, 3, 5, std)
runDeviationComparisonSet(folderName +  "/qtable80k3", 80000, 3, 5, std)
runDeviationComparisonSet(folderName +  "/qtable100k3", 100000, 3, 5, std)

runDeviationComparisonSet(folderName +  "/qtable5k5", 5000, 5, 5, std)
runDeviationComparisonSet(folderName +  "/qtable10k5", 10000, 5, 5, std)
runDeviationComparisonSet(folderName +  "/qtable20k5", 20000, 5, 5, std)
runDeviationComparisonSet(folderName +  "/qtable30k5", 30000, 5, 5, std)
runDeviationComparisonSet(folderName +  "/qtable40k5", 40000, 5, 5, std)
runDeviationComparisonSet(folderName +  "/qtable50k5", 50000, 5, 5, std)
runDeviationComparisonSet(folderName +  "/qtable80k5", 80000, 5, 5, std)
runDeviationComparisonSet(folderName +  "/qtable100k5", 100000, 5, 5, std)

runDeviationComparisonSet(folderName +  "/qtable5k7", 5000, 7, 5, std)
runDeviationComparisonSet(folderName +  "/qtable10k7", 10000, 7, 5, std)
runDeviationComparisonSet(folderName +  "/qtable20k7", 20000, 7, 5, std)
runDeviationComparisonSet(folderName +  "/qtable30k7", 30000, 7, 5, std)
runDeviationComparisonSet(folderName +  "/qtable40k7", 40000, 7, 5, std)
runDeviationComparisonSet(folderName +  "/qtable50k7", 50000, 7, 5, std)
runDeviationComparisonSet(folderName +  "/qtable80k7", 80000, 7, 5, std)
runDeviationComparisonSet(folderName +  "/qtable100k7", 100000, 7, 5, std)

runDeviationComparisonSet(folderName +  "/qtable5k9", 5000, 9, 5, std)
runDeviationComparisonSet(folderName +  "/qtable10k9", 10000, 9, 5, std)
runDeviationComparisonSet(folderName +  "/qtable20k9", 20000, 9, 5, std)
