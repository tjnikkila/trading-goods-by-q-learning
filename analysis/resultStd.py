import os
import json
import ast
from pprint import pprint
import random
import numpy as np
import argparse
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", "../standardDeviation.py")
foo = importlib.util.module_from_spec(spec)
spec.loader.exec_module(foo)


parser = argparse.ArgumentParser()
parser.add_argument("-fp", help="file path",
                    type=str, required=True)

args = parser.parse_args()
fp = args.fp

dirtyHackFineNames = [fp + "1.json",fp + "2.json",fp + "3.json",fp + "4.json",fp + "5.json"]
std = foo.TradeStd()(dirtyHackFineNames)
print("Normalized, combined, mean standard deviation per sample.")
pprint(std)
