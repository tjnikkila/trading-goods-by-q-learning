import sys
sys.path.insert(0,'../')
import unittest
from tradeAction import getActions, DefaultActions
from tradeState import getStates
from pprint import pprint

# python3 -m unittest *
#run on terminal: python3 -m unittest tradeActionTest.TestTradeAction
class TestTradeAction(unittest.TestCase):
    customActions = {'B': [{'state': 'pc', 'transitions': [{'name': 'laptop', 'prob': 0.7}, {'name': 'mobile', 'prob': 0.3}]}], 'C': [{'state': 'calculator', 'transitions': [{'name': 'mobile', 'prob': 0.8}, {'name': 'calculator', 'prob': 0.2}]}]}

    def test_withCustomAction(self):
        actions = getActions(getStates(None), self.customActions)
        self.assertEqual(actions.dealerB['stateTransition']['pc'], self.customActions['B'][0]['transitions'])
        self.assertEqual(actions.dealerC['stateTransition']['calculator'], self.customActions['C'][0]['transitions'])

    def test_noCustomAction(self):
        states = getStates(None)
        defaultActions = DefaultActions(states)
        actions = getActions(states, None)
        self.assertEqual(actions.dealerB['stateTransition']['pc'], defaultActions.dealerB['stateTransition']['pc'])
        self.assertEqual(actions.dealerC['stateTransition']['calculator'], defaultActions.dealerC['stateTransition']['calculator'])

if __name__ == '__main__':
    unittest.main()
