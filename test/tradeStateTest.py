import sys
sys.path.insert(0,'../')
import unittest
from tradeState import getStates
from pprint import pprint

#run on terminal: python3 -m unittest tradeServiceTest.TestTradeService
class TestTradeState(unittest.TestCase):
    laptop = {'name': 'laptop', 'reward': 0, 'index': 1}
    def test_customRewardNone(self):
        states = getStates(None)
        self.assertEqual(states.createTailoredState(states.laptop, None), states.laptop)

    def test_customRewardOtherState(self):
        customReward = {'mobile': 120}
        states = getStates(customReward)
        self.assertEqual(states.createTailoredState(states.laptop, customReward), states.laptop)

    def test_createTailoredState(self):
        customReward = {'laptop': 121}
        customLaptop = {'name': 'laptop', 'reward': 121, 'index': 1}
        states = getStates(customReward)
        self.assertEqual(states.createTailoredState(states.laptop, customReward), customLaptop)

    def test_customStates(self):
        customReward = {'laptop': 121}
        customLaptop = {'name': 'laptop', 'reward': 121, 'index': 1}
        states = getStates(customReward)
        self.assertEqual(next(x for x in states.all if x['name'] == 'laptop'), customLaptop)

if __name__ == '__main__':
    unittest.main()
