import sys
sys.path.insert(0,'../')
import unittest
from tradeService import updatedQTable
from pprint import pprint

# python3 -m unittest *
#run on terminal: python3 -m unittest tradeServiceTest.TestTradeService
class TestTradeService(unittest.TestCase):
    mockedFrom = {'name': 'mockedFrom', 'reward': 0, 'index': 0}
    mockedTo = {'name': 'mockedTo', 'reward': 100, 'index': 1}

    mockedDealer = {
        'name': 'Mocked',
        'decription': 'mocked for test',
        'index': 1,
        'stateTransition': {
            mockedTo['name']: [{'name': mockedFrom['name'], 'prob': 1}],
            mockedFrom['name']: [],
        }}

    def test_onlyReward(self):
        qTableBefore = [[0,0],
                        [0,0]]

        qTableAfter = [ [0,20],
                        [0,0]]

        alfa = 0.2
        gamma = 0.3
        updated = updatedQTable(qTableBefore, self.mockedFrom, self.mockedTo, self.mockedDealer, alfa, gamma)
        self.assertEqual(updated, qTableAfter)

    def test_QUpdate(self):
        qTableBefore = [[1.3,2.1],
                        [6.8,0.7]]

        qTableAfter = [ [1.3,22.088],
                        [6.8,0.7]]

        #Q'[s, a] = (1 - α) · Q[s, a] + α · (r + γ · Q[s', argmaxa'(Q[s', a'])])
        # (1-0.2)*2.1 + 0.2*(100 + 0.3*6.8) = 22.088
        alfa = 0.2
        gamma = 0.3
        updated = updatedQTable(qTableBefore, self.mockedFrom, self.mockedTo, self.mockedDealer, alfa, gamma)
        self.assertEqual(updated, qTableAfter)




if __name__ == '__main__':
    unittest.main()
