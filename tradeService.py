import random
import numpy as np

def updatedQTable(qTable, origState, newState, action, alpha, gamma):
    # q-table update formula:
    # Q'[s, a] = (1 - α) · Q[s, a] + α · (r + γ · Q[s', argmaxa'(Q[s', a'])])
    qTable[origState['index']][action['index']] = (1-alpha)*qTable[origState['index']][action['index']]  \
    + alpha * (newState['reward'] + gamma * max(qTable[newState['index']]))
    return qTable

def getNextAction(currentState, qTable, actions, exploitationProb):
    possibleActions = []

    for action in actions.all:
        if action['stateTransition'][currentState['name']] != []:
            possibleActions.append({'action': action, 'qValue':qTable[currentState['index']][action['index']]})


    if random.random() > exploitationProb:
        return random.choice (possibleActions)['action']
    else:
        def getQValue(action):
            return action['qValue']
        greatestQValAction = sorted(possibleActions, key=getQValue)[-1:][0]["action"]
        return greatestQValAction

def getNextState(currentState, action, states):
    def getPropability(actionProb):
        return actionProb['prob']
    possibleNextStates = action['stateTransition'][currentState['name']]
    probs = [a for a in map(getPropability, possibleNextStates)]

    nextStateIndex = np.random.choice(len(possibleNextStates), 1, p=probs)[0]
    stateName = possibleNextStates[nextStateIndex]['name']
    st = next(s for s in states.all if s['name'] == stateName)
    return st

def validateSteps(currentState, qTable, states, actions):
    if currentState['name'] != states.finalState['name']:
        action = getNextAction(currentState, qTable, actions, 1)
        if action['name'] != currentState['bestAction']:
            return False
        else:
            nextState = getNextState(currentState, action, states)
            return validateSteps(nextState, qTable, states, actions)
    else:
        return True

def trainQTableForEpic(qTable, initState, states, actions, exploitationProb, alpha, gamma):
    stepsInEpic = 0
    # Max steps defined for a a case program never enters final state.
    maxStepsInEpic = 1000
    currentState = initState
    while currentState['name'] != states.finalState['name'] and stepsInEpic < maxStepsInEpic:
        action = getNextAction(currentState, qTable, actions, exploitationProb)
        nextState = getNextState(currentState, action, states)
        qTable = updatedQTable(qTable, currentState, nextState, action, alpha, gamma)
        currentState = nextState
        stepsInEpic += 1

    if(stepsInEpic >= maxStepsInEpic):
        print("Epic exists because max steps per epic exceeded. Max steps is ", maxStepsInEpic)

    return qTable
